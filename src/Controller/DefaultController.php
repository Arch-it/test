<?php

namespace App\Controller;

use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class DefaultController extends AbstractController
{
    /**
     * @Route("/frontend", name="test_frontend")
     */
    public function frontend(): Response
    {
        return $this->render('frontend.html.twig');
    }

    /**
     * @Route("/backend", name="test_backend")
     * @param Request         $request
     * @param LoggerInterface $logger
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function backend(Request $request, LoggerInterface $logger): Response
    {
        $date = $request->get('date');
        if ($date !== null) {
            $logger->info($date);
        }

        return $this->redirectToRoute('test_frontend');
    }

    /**
     * @Route("/current_date", name="get_current_date")
     */
    public function getDate(): Response
    {
        return new JsonResponse(['current_date' => date('d.m.Y H:i:s')]);
    }
}
